%% Erlang Syntax
x=1.
% ** exception error: no match of right hand side value 1
%% variables must start from capital letter
X=1.
% 1
A=1.
% 1

X=X+5.
% ** exception error: no match of right hand side value 6
%% variables are immutable and already defined cannot be reassigned
B=X+5.
% 6
Abc = 12.
% 12
