-module(hello).

  %% API
  -export([hello_world/0]).
  hello_world() -> io:fwrite("hello world\n").

  %% to compile run following command:
  % > c(hello).
  % {ok,hello}
  %% to run execute following command:
  % > hello:hello_world().
  % hello world
  % ok
