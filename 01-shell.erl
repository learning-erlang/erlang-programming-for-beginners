%% Erlang basics
%% Strings
B = "hello world".
% "hello world"
B = [104,101,108,108,111,32,119,111,114,108,100].
% "hello world"
B = [$h,$e,$l,$l,$o,$ ,$w,$o,$r,$l,$d].
% "hello world"

%% comparison
13>2.
% true
18.2>=19.
% false
3==3.
% true
4=/=4.
% false
4=/=5.
% true

%% atoms
abc.
% abc
abc == def.
% false
abc == abc.
% true

%% tuples
{abc,def,ghi}.
% {abc,def,ghi}
{email, 'example@example.org'}.
% {email,'example@example.org'}
element(3, {abc,def,ghi,jkl}).
% ghi
setelement(3, {abc,def,ghi,jkl},mno).
% {abc,def,mno,jkl}
{abc,def} == {abc,def}.
% true
{abc,def} == {abc,mno}.
%false
