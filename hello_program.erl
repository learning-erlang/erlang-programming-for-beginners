-module(hello_program).
-author("luhtonen").

%% API
-export([hello/0]).

hello() ->
  receive
    {FromPID, Who} ->
      case Who of
        arno -> FromPID ! "Terve Arno.";
        lauri -> FromPID ! "Terve Lauri";
        kristian -> FromPID ! "Terve Kristian";
        karoliina -> FromPID ! "Moi Karoliina";
        _ -> FromPID ! "Tuntematon valinta"
      end,
      hello()
  end.

%% compile program
%% > c(hello_program).

%% call program
%%1> Pid = spawn(fun hello_program:hello/0).
%%<0.59.0>
%%2> Pid ! arno
%%2> .
%%arno
%%3> Pid ! {self(),arno}.
%%{<0.57.0>,arno}
%%4> Pid ! {self(),arno},
%%4> receive
%%4> Response ->
%%4> Response
%%4> end.
%%"Terve Arno."
