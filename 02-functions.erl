%% creating functions
F = fun(X) -> 2*X end.
% #Fun<erl_eval.6.52032458>
F(4).
% 8
F(2).
% 4

%% calling functions
lists:sort([4,5,6,1]).
% [1,4,5,6]
lists:sort([b,c,a,d]).
% [a,b,c,d]

Hw = fun(X) -> "hello " + X end.
% #Fun<erl_eval.6.52032458>
Hw("Edu").
% ** exception error: an error occurred when evaluating an arithmetic expression
%      in operator  +/2
%         called as "hello " + "Edu"

%% fixed version
Hw2 = fun(X) -> string:concat("hello ",X) end.
% #Fun<erl_eval.6.52032458>
Hw2("Edu").
% "hello Edu"
